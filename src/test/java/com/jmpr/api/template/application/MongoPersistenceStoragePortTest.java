package com.jmpr.api.template.application;

import com.jmpr.api.template.application.bean.dto.ItemDto;
import com.jmpr.api.template.application.ports.ItemPersistencyPort;
import com.jmpr.api.template.framework.annotation.ValidItemId;
import com.jmpr.api.template.framework.entity.ItemEntity;
import com.jmpr.api.template.framework.exception.InvalidItemException;
import com.jmpr.api.template.framework.exception.ItemNotFoundException;
import com.jmpr.api.template.framework.validators.ValidItemIdValidator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.Collections;
import java.util.Map;
import java.util.regex.Pattern;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@RunWith(SpringRunner.class)
@ActiveProfiles({"test", "test-mongo"})
public class MongoPersistenceStoragePortTest {
    @Autowired
    private ItemPersistencyPort itemPersistencyPort;

    @Autowired
    private MongoTemplate mongoTemplate;

    @Before
    public void setUp() {
        mongoTemplate.dropCollection(ItemEntity.class);
    }

    @Test
    public void contextLoaded() {
        assertThat(itemPersistencyPort).isNotNull();
    }

    @Test
    public void getItems() {
        // Arrange
        ItemEntity itemEntity1 = ItemEntity
                .builder()
                .id("1")
                .name("testName1")
                .description("testDescription1")
                .tags(Arrays.asList("tag11", "tag12"))
                .build();

        ItemEntity itemEntity2 = ItemEntity
                .builder()
                .id("2")
                .name("testName2")
                .description("testDescription2")
                .tags(Arrays.asList("tag21", "tag22"))
                .build();
        populateDatabase(itemEntity1, itemEntity2);

        // Act
        Map<String, ItemDto> items = itemPersistencyPort.getItems();

        // Assert
        assertThat(items).isNotNull();
        assertThat(items.values().size()).isEqualTo(2);
        assertThat(items.get("1")).isNotNull();
        assertThat(items.get("1").getName()).isEqualTo(itemEntity1.getName());
        assertThat(items.get("1").getDescription()).isEqualTo(itemEntity1.getDescription());
        assertThat(items.get("1").getTags()).isNotNull();
        assertThat(items.get("1").getTags().size()).isEqualTo(2);
        assertThat(items.get("1").getTags().get(0)).isEqualTo(itemEntity1.getTags().get(0));
        assertThat(items.get("1").getTags().get(1)).isEqualTo(itemEntity1.getTags().get(1));
        assertThat(items.get("2")).isNotNull();
        assertThat(items.get("2").getName()).isEqualTo(itemEntity2.getName());
        assertThat(items.get("2").getDescription()).isEqualTo(itemEntity2.getDescription());
        assertThat(items.get("2").getTags()).isNotNull();
        assertThat(items.get("2").getTags().size()).isEqualTo(2);
        assertThat(items.get("2").getTags().get(0)).isEqualTo(itemEntity2.getTags().get(0));
        assertThat(items.get("2").getTags().get(1)).isEqualTo(itemEntity2.getTags().get(1));

    }

    @Test
    public void getItemsEmpty() {
        // Arrange

        // Act
        Map<String, ItemDto> items = itemPersistencyPort.getItems();

        // Assert
        assertThat(items).isNotNull();
        assertThat(items.values().size()).isEqualTo(0);
    }

    @Test(expected = InvalidItemException.class)
    public void getItemDetailNull() {
        itemPersistencyPort.getItem(null);
    }

    @Test(expected = ItemNotFoundException.class)
    public void getItemDetailNotFound() {
        itemPersistencyPort.getItem("NonExistent");
    }

    @Test
    public void getItemDetailWithIdSet() {
        // Arrange
        ItemEntity itemToSave = ItemEntity
                .builder()
                .id("123")
                .description("testDescription")
                .name("testName")
                .tags(Arrays.asList("tag1", "tag2"))
                .build();
        populateDatabase(itemToSave);

        // Act
        ItemDto retrievedItem = itemPersistencyPort.getItem(itemToSave.getId());

        // Assert
        assertThat(retrievedItem).isNotNull();
        assertThat(retrievedItem.getId()).isEqualTo(itemToSave.getId());
        assertThat(retrievedItem.getName()).isEqualTo(itemToSave.getName());
        assertThat(retrievedItem.getDescription()).isEqualTo(itemToSave.getDescription());
        assertThat(retrievedItem.getTags()).isNotNull();
        assertThat(retrievedItem.getTags().size()).isEqualTo(2);
        assertThat(retrievedItem.getTags().get(0)).isEqualTo(itemToSave.getTags().get(0));
        assertThat(retrievedItem.getTags().get(1)).isEqualTo(itemToSave.getTags().get(1));
    }

    @Test
    public void getItemDetailWithIdGenerated() {
        // Arrange
        ItemEntity itemToSave = ItemEntity
                .builder()
                .description("testDescription")
                .name("testName")
                .tags(Arrays.asList("tag1", "tag2"))
                .build();
        populateDatabase(itemToSave);

        // Act
        ItemDto retrievedItem = itemPersistencyPort.getItem(itemToSave.getId());

        // Assert
        assertThat(retrievedItem).isNotNull();
        assertThat(retrievedItem.getId()).matches(Pattern.compile(".{24}"));
        assertThat(retrievedItem.getName()).isEqualTo(itemToSave.getName());
        assertThat(retrievedItem.getDescription()).isEqualTo(itemToSave.getDescription());
        assertThat(retrievedItem.getTags()).isNotNull();
        assertThat(retrievedItem.getTags().size()).isEqualTo(2);
        assertThat(retrievedItem.getTags().get(0)).isEqualTo(itemToSave.getTags().get(0));
        assertThat(retrievedItem.getTags().get(1)).isEqualTo(itemToSave.getTags().get(1));
    }

    @Test(expected = InvalidItemException.class)
    public void storeItemNull() {
        itemPersistencyPort.storeItem(null);
    }

    @Test(expected = InvalidItemException.class)
    public void storeItemExistentItem() {
        // Arrange
        ItemEntity existentItem = ItemEntity
                .builder()
                .description("testDescription")
                .name("testName")
                .tags(Arrays.asList("tag1", "tag2"))
                .build();
        populateDatabase(existentItem);

        ItemDto itemToSave = ItemDto
                .builder()
                .id(existentItem.getId())
                .name("anotherName")
                .description("anotherDescription")
                .tags(Arrays.asList("anotherTag1", "anotherTag2"))
                .build();

        // Act
        itemPersistencyPort.storeItem(itemToSave);
    }

    @Test
    public void storeItemWithIdNonExisting() {
        // Arrange
        ItemDto itemToSave = ItemDto
                .builder()
                .id("123456789012345678901234")
                .name("anotherName")
                .description("anotherDescription")
                .tags(Arrays.asList("anotherTag1", "anotherTag2"))
                .build();

        // Act
        ItemDto savedItem = itemPersistencyPort.storeItem(itemToSave);

        // Assert
        ItemDto storedItem = itemPersistencyPort.getItem(savedItem.getId());
        assertThat(storedItem).isNotNull();
        assertThat(storedItem.getId()).matches(Pattern.compile(ValidItemIdValidator.UUID_REGEX));
        assertThat(storedItem.getName()).isEqualTo(itemToSave.getName());
        assertThat(storedItem.getDescription()).isEqualTo(itemToSave.getDescription());
        assertThat(storedItem.getTags()).isNotNull();
        assertThat(storedItem.getTags().size()).isEqualTo(itemToSave.getTags().size());
        assertThat(storedItem.getTags().get(0)).isEqualTo(itemToSave.getTags().get(0));
        assertThat(storedItem.getTags().get(1)).isEqualTo(itemToSave.getTags().get(1));
    }

    @Test
    public void storeItemWithoutId() {
        // Arrange
        ItemDto itemToSave = ItemDto
                .builder()
                .name("anotherName")
                .description("anotherDescription")
                .tags(Arrays.asList("anotherTag1", "anotherTag2"))
                .build();

        // Act
        ItemDto savedItem = itemPersistencyPort.storeItem(itemToSave);

        // Assert
        ItemDto storedItem = itemPersistencyPort.getItem(savedItem.getId());
        assertThat(storedItem).isNotNull();
        assertThat(storedItem.getId()).matches(Pattern.compile(ValidItemIdValidator.UUID_REGEX));
        assertThat(storedItem.getName()).isEqualTo(itemToSave.getName());
        assertThat(storedItem.getDescription()).isEqualTo(itemToSave.getDescription());
        assertThat(storedItem.getTags()).isNotNull();
        assertThat(storedItem.getTags().size()).isEqualTo(itemToSave.getTags().size());
        assertThat(storedItem.getTags().get(0)).isEqualTo(itemToSave.getTags().get(0));
        assertThat(storedItem.getTags().get(1)).isEqualTo(itemToSave.getTags().get(1));
    }

    @Test(expected = InvalidItemException.class)
    public void deleteItemNull() {
        itemPersistencyPort.deleteItem(null);
    }

    @Test(expected = ItemNotFoundException.class)
    public void deleteItemNonExistent() {
        itemPersistencyPort.deleteItem("non-existent");
    }

    @Test
    public void deleteItem() {
        // Arrange
        ItemEntity itemInDb = ItemEntity.builder()
                .name("testName")
                .description("testDescription")
                .tags(Arrays.asList("testTag1", "testTag2"))
                .build();
        populateDatabase(itemInDb);

        // Act
        ItemDto deletedItem = itemPersistencyPort.deleteItem(itemInDb.getId());

        // Assert
        assertThat(deletedItem).isNotNull();
        try {
            itemPersistencyPort.getItem(deletedItem.getId());
        } catch (Exception e) {
            assertThat(e).isInstanceOf(ItemNotFoundException.class);
        }
    }

    @Test(expected = InvalidItemException.class)
    public void updateItemNull() {
        itemPersistencyPort.updateItem(null);
    }

    @Test(expected = ItemNotFoundException.class)
    public void updateItemNonExistent() {
        itemPersistencyPort.updateItem(ItemDto.builder().id("non-existent").build());
    }

    @Test
    public void updateItem() {
        // Arrange
        ItemEntity existingItem = ItemEntity.builder()
                .id("1234")
                .name("MyName")
                .description("MyDescription")
                .tags(Collections.emptyList())
                .build();
        populateDatabase(existingItem);

        // Act
        ItemDto updatedItem = itemPersistencyPort.updateItem(ItemDto.builder()
                .id("1234")
                .name("ModifiedName")
                .build());
        ItemDto storedItem = itemPersistencyPort.getItem(updatedItem.getId());

        // Assert
        assertThat(updatedItem).isNotNull();
        assertThat(storedItem).isNotNull();
        assertThat(storedItem).isEqualToComparingFieldByField(updatedItem);

    }

    private void populateDatabase(ItemEntity... items) {
        for (ItemEntity entity : items) {
            mongoTemplate.save(entity);
        }
    }
}
