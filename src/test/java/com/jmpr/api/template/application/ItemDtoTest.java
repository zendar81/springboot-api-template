package com.jmpr.api.template.application;

import com.jmpr.api.template.application.bean.dto.ItemDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(JUnit4.class)
public class ItemDtoTest {
    ItemDto source = createItem();

    @Test
    public void isEqualToNullTarget() {
        assertThat(source.isEqualTo(null)).isFalse();
    }

    @Test
    public void isEqualToSameTarget() {
        assertThat(source.isEqualTo(createItem())).isTrue();
    }

    @Test
    public void isEqualToTargetWithDifferentId() {
        ItemDto targetItem = createItem();
        targetItem.setId("000");

        assertThat(source.isEqualTo(targetItem)).isFalse();
    }

    @Test
    public void isEqualToTargetWithDifferentName() {
        ItemDto targetItem = createItem();
        targetItem.setName("Another name");

        assertThat(source.isEqualTo(targetItem)).isFalse();
    }

    @Test
    public void isEqualToTargetWithDifferentDescription() {
        ItemDto targetItem = createItem();
        targetItem.setDescription("Another description");

        assertThat(source.isEqualTo(targetItem)).isFalse();
    }

    @Test
    public void isEqualToTargetWithBothTagsNull() {
        ItemDto targetItem = createItem();
        ItemDto sourceItem = createItem();
        targetItem.setTags(null);
        sourceItem.setTags(null);

        assertThat(sourceItem.isEqualTo(targetItem)).isTrue();
    }


    @Test
    public void isEqualToTargetWithTargetTagsNull() {
        ItemDto targetItem = createItem();
        source.setTags(null);

        assertThat(source.isEqualTo(targetItem)).isFalse();
    }

    @Test
    public void isEqualToTargetWithSourceTagsNull() {
        ItemDto targetItem = createItem();
        targetItem.setTags(null);

        assertThat(source.isEqualTo(targetItem)).isFalse();
    }

    @Test
    public void isEqualToTargetWithDifferentTags() {
        ItemDto targetItem = createItem();
        targetItem.setTags(Arrays.asList("t1", "t2"));

        assertThat(source.isEqualTo(targetItem)).isFalse();
    }

    @Test
    public void mergeWithSame() {
        ItemDto mergedItem = source.mergeWith(source);

        assertThat(source.getId()).isEqualTo(mergedItem.getId());
        assertThat(source.getName()).isEqualTo(mergedItem.getName());
        assertThat(source.getDescription()).isEqualTo(mergedItem.getDescription());
        assertThat(source.getTags()).isNotNull();
        assertThat(source.getTags().size()).isEqualTo(mergedItem.getTags().size());
        assertThat(source.getTags().containsAll(mergedItem.getTags())).isTrue();

    }

    @Test
    public void mergeWithTargetNull() {
        ItemDto mergedItem = source.mergeWith(null);

        assertThat(source.getId()).isEqualTo(mergedItem.getId());
        assertThat(source.getName()).isEqualTo(mergedItem.getName());
        assertThat(source.getDescription()).isEqualTo(mergedItem.getDescription());
        assertThat(source.getTags()).isNotNull();
        assertThat(source.getTags().size()).isEqualTo(mergedItem.getTags().size());
        assertThat(source.getTags().containsAll(mergedItem.getTags())).isTrue();

    }

    @Test
    public void mergeWithTargetEmpty() {
        ItemDto mergedItem = source.mergeWith(ItemDto.builder().build());

        assertThat(source.getId()).isEqualTo(mergedItem.getId());
        assertThat(source.getName()).isEqualTo(mergedItem.getName());
        assertThat(source.getDescription()).isEqualTo(mergedItem.getDescription());
        assertThat(source.getTags()).isNotNull();
        assertThat(source.getTags().size()).isEqualTo(mergedItem.getTags().size());
        assertThat(source.getTags().containsAll(mergedItem.getTags())).isTrue();
    }

    private ItemDto createItem() {
        return ItemDto.builder()
                .id("1234567890")
                .name("testName")
                .description("testDescription")
                .tags(Arrays.asList("testTag1", "testTag2"))
                .build();
    }
}
