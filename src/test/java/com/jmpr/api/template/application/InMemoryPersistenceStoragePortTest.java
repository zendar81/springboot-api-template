package com.jmpr.api.template.application;

import com.jmpr.api.template.application.ports.ItemPersistencyPort;
import com.jmpr.api.template.application.bean.dto.ItemDto;
import com.jmpr.api.template.domain.Item;
import com.jmpr.api.template.framework.exception.InvalidItemException;
import com.jmpr.api.template.framework.exception.ItemNotFoundException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@RunWith(SpringRunner.class)
@ActiveProfiles({"test-memory", "test"})
public class InMemoryPersistenceStoragePortTest {
    @Autowired
    private ItemPersistencyPort itemPersistencyPort;

    @Test
    public void contextLoaded() {
        assertThat(itemPersistencyPort).isNotNull();
    }

    @Test(expected = InvalidItemException.class)
    public void insertEmptyItem() {
        itemPersistencyPort.storeItem(null);
    }

    @Test
    public void insertAndGetItem() {
        ItemDto itemToInsert = insertTestItem();

        Map<String, ItemDto> items = itemPersistencyPort.getItems();
        assertThat(items.isEmpty()).isFalse();

        ItemDto insertedItem = items.values().stream().filter(item -> item.getId().equals(itemToInsert.getId())).findFirst().get();

        assertThat(insertedItem).isNotNull();
        assertThat(insertedItem.getId()).isNotNull();
        assertThat(insertedItem.getName()).isEqualTo(itemToInsert.getName());
        assertThat(itemToInsert.getDescription()).isEqualTo(itemToInsert.getDescription());
        assertThat(itemToInsert.getTags()).isNotNull();
        assertThat(itemToInsert.getTags().size()).isEqualTo(itemToInsert.getTags().size());
        assertThat(itemToInsert.getTags().get(0)).isEqualTo(itemToInsert.getTags().get(0));
        assertThat(itemToInsert.getTags().get(1)).isEqualTo(itemToInsert.getTags().get(1));
    }

    @Test
    public void deleteExistingItem(){
        // Arrange
        ItemDto itemToInsert = insertTestItem();

        // Act
        itemPersistencyPort.deleteItem(itemToInsert.getId());
    }

    @Test(expected = InvalidItemException.class)
    public void deleteItemNull() {
        itemPersistencyPort.deleteItem(null);
    }

    @Test(expected = ItemNotFoundException.class)
    public void deleteItemNotFound() {
        itemPersistencyPort.deleteItem("1234567890");
    }

    @Test
    public void getItemDetail() {
        // Arrange
        ItemDto itemToInsert = insertTestItem();

        // Act
        ItemDto item = itemPersistencyPort.getItem(itemToInsert.getId());

        // Arrange
        assertThat(item).isNotNull();
    }

    @Test(expected = InvalidItemException.class)
    public void getItemDetailNullId() {
        itemPersistencyPort.getItem(null);
    }

    @Test(expected = InvalidItemException.class)
    public void updateItemNullId() {
        itemPersistencyPort.updateItem(null);
    }

    @Test(expected = ItemNotFoundException.class)
    public void updateItemNotFound() {
        ItemDto itemToUpdate = ItemDto.builder().id("1234567890").build();

        itemPersistencyPort.updateItem(itemToUpdate);
    }

    @Test
    public void updateItemFullUpdate() {
        ItemDto itemToInsert = insertTestItem();
        itemToInsert.setDescription("modifiedName");

        ItemDto modifiedItem = itemPersistencyPort.updateItem(itemToInsert);
        ItemDto storedModifiedItem = itemPersistencyPort.getItem(modifiedItem.getId());

        assertThat(modifiedItem).isNotNull();
        assertThat(storedModifiedItem).isNotNull();
        assertThat(modifiedItem).isNotSameAs(storedModifiedItem);
        assertThat(storedModifiedItem.getDescription()).isEqualTo(itemToInsert.getDescription());
        assertThat(storedModifiedItem.getId()).isEqualTo(itemToInsert.getId());
        assertThat(storedModifiedItem.getName()).isEqualTo(itemToInsert.getName());
        assertThat(storedModifiedItem.getTags()).isNotNull();
        assertThat(modifiedItem.getTags()).isNotNull();
        assertThat(storedModifiedItem.getTags().size()).isEqualTo(modifiedItem.getTags().size());
        assertThat(storedModifiedItem.getTags()).containsAll(modifiedItem.getTags());
    }

    @Test
    public void updateItemPartialUpdate() {
        ItemDto insertedItem = insertTestItem();

        ItemDto itemToUpdate = ItemDto
                .builder()
                .id(insertedItem.getId())
                .name("modifiedName")
                .build();
        ItemDto modifiedItem = itemPersistencyPort.updateItem(itemToUpdate);
        ItemDto storedModifiedItem = itemPersistencyPort.getItem(modifiedItem.getId());

        assertThat(modifiedItem).isNotNull();
        assertThat(storedModifiedItem).isNotNull();
        assertThat(modifiedItem).isNotSameAs(storedModifiedItem);
        assertThat(storedModifiedItem.getDescription()).isEqualTo(insertedItem.getDescription());
        assertThat(storedModifiedItem.getId()).isEqualTo(insertedItem.getId());
        assertThat(storedModifiedItem.getName()).isEqualTo(modifiedItem.getName());
        assertThat(storedModifiedItem.getTags()).isNotNull();
        assertThat(modifiedItem.getTags()).isNotNull();
        assertThat(storedModifiedItem.getTags().size()).isEqualTo(modifiedItem.getTags().size());
        assertThat(storedModifiedItem.getTags()).containsAll(modifiedItem.getTags());
    }

    private ItemDto insertTestItem() {
        ItemDto itemToInsert = ItemDto.builder()
                .name("TestName")
                .description("TestDescription")
                .tags(Arrays.asList("tag1", "tag2"))
                .build();

        itemPersistencyPort.storeItem(itemToInsert);
        return itemToInsert;
    }
}
