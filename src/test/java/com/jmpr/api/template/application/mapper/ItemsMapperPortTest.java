package com.jmpr.api.template.application.mapper;

import com.jmpr.api.template.application.bean.dto.ItemDto;
import com.jmpr.api.template.application.ports.ItemsMapperPort;
import com.jmpr.api.template.domain.Item;
import com.jmpr.api.template.framework.controller.bean.CreateItemRequest;
import com.jmpr.api.template.framework.controller.bean.CreateItemResponse;
import com.jmpr.api.template.framework.controller.bean.UpdateItemRequest;
import com.jmpr.api.template.framework.controller.bean.UpdateItemResponse;
import com.jmpr.api.template.framework.entity.ItemEntity;
import com.jmpr.api.template.framework.exception.InvalidItemException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@RunWith(SpringRunner.class)
@ActiveProfiles({"test", "test-memory"})
public class ItemsMapperPortTest {
    @Autowired
    private ItemsMapperPort itemsMapperPort;

    @Value("${application.domain}")
    private String applicationDomain;

    @Test(expected = InvalidItemException.class)
    public void mapListItemsResponseToDtoEmpty() {
        itemsMapperPort.mapListItemsResponseToDto(null);
    }

    @Test
    public void mapListItemsResponseToDto() {
        // Arrange
        Map<String, ItemDto> serviceItems = new HashMap<>();
        ItemDto itemDto1 = createTestItem();
        serviceItems.put("1234567890", itemDto1);

        List<Item> expectedResponse = Arrays.asList(Item.builder()
                .id("id")
                .name(itemDto1.getName())
                .description(itemDto1.getDescription())
                .tags(itemDto1.getTags())
                .build());


        // Act
        List<ItemDto> response = itemsMapperPort.mapListItemsResponseToDto(serviceItems);

        // Assert
        assertThat(response).isNotNull();
        assertThat(response.size()).isEqualTo(1);
        assertThat(response.get(0).getName()).isEqualTo(expectedResponse.get(0).getName());
        assertThat(response.get(0).getDescription()).isEqualTo(expectedResponse.get(0).getDescription());
        assertThat(response.get(0).getTags()).isNotNull();
        assertThat(response.get(0).getTags().size()).isEqualTo(2);
        assertThat(response.get(0).getTags().get(0)).isEqualTo(expectedResponse.get(0).getTags().get(0));
        assertThat(response.get(0).getTags().get(1)).isEqualTo(expectedResponse.get(0).getTags().get(1));

    }

    @Test(expected = InvalidItemException.class)
    public void mapCreateItemRequestToDtoNull() {
        itemsMapperPort.mapCreateItemRequestToDto(null);
    }

    @Test
    public void mapCreateItemRequestToDto() {
        CreateItemRequest request = CreateItemRequest
                .builder()
                .name("testName")
                .description("testDescription")
                .tags(Arrays.asList("testTag1", "testTag2"))
                .build();

        ItemDto mappedRequest = itemsMapperPort.mapCreateItemRequestToDto(request);
        assertThat(mappedRequest).isNotNull();
        assertThat(mappedRequest).hasFieldOrPropertyWithValue("name", "testName");
        assertThat(mappedRequest).hasFieldOrPropertyWithValue("description", "testDescription");
        assertThat(mappedRequest.getTags()).isEqualTo(request.getTags());
    }

    @Test(expected = InvalidItemException.class)
    public void mapCreateItemDtoToResponseNull() {
        itemsMapperPort.mapCreateItemDtoToResponse(null);
    }

    @Test
    public void mapCreateItemDtoToResponse() {
        ItemDto itemDto = createTestItem();

        CreateItemResponse response = itemsMapperPort.mapCreateItemDtoToResponse(itemDto);
        assertThat(response.getId()).isEqualTo(itemDto.getId());
        assertThat(response.getLinks()).isNotNull();
        assertThat(response.getLinks().getDetail()).isNotNull();
        assertThat(response.getLinks().getDetail()).contains(applicationDomain + "/items/1234567890");
    }

    @Test(expected = InvalidItemException.class)
    public void mapGetItemDetailDtoNull() {
        itemsMapperPort.mapGetItemDetailDto(null);
    }

    @Test
    public void mapGetItemDetailDto() {
        // Arrange
        ItemDto itemToMap = createTestItem();

        // Act
        Item mappedItem = itemsMapperPort.mapGetItemDetailDto(itemToMap);

        // Assert
        assertThat(mappedItem.getId()).isEqualTo(itemToMap.getId());
        assertThat(mappedItem.getName()).isEqualTo(itemToMap.getName());
        assertThat(mappedItem.getDescription()).isEqualTo(itemToMap.getDescription());
        assertThat(mappedItem.getTags()).isNotNull();
        assertThat(mappedItem.getTags().size()).isEqualTo(2);
        assertThat(mappedItem.getTags().get(0)).isEqualTo(itemToMap.getTags().get(0));
        assertThat(mappedItem.getTags().get(1)).isEqualTo(itemToMap.getTags().get(1));
    }

    @Test(expected = InvalidItemException.class)
    public void mapUpdateItemRequestToDtoNull() {
        // Act
        itemsMapperPort.mapUpdateItemRequestToDto(null);
    }


    @Test
    public void mapUpdateItemRequestToDtoEmpty() {
        // Arrange
        UpdateItemRequest request = UpdateItemRequest.builder().build();
        ItemDto expectedMappedRequest = ItemDto.builder().build();

        // Act
        ItemDto actualMappedRequest = itemsMapperPort.mapUpdateItemRequestToDto(request);

        // Assert
        assertThat(expectedMappedRequest.isEqualTo(actualMappedRequest)).isTrue();
    }

    @Test
    public void mapUpdateItemRequestToDto() {
        // Arrange
        UpdateItemRequest request = UpdateItemRequest.builder()
                .name("testName")
                .description("testDescription")
                .tags(Arrays.asList("testTag1", "testTag2"))
                .build();
        ItemDto expectedMappedRequest = ItemDto.builder()
                .name(request.getName())
                .description(request.getDescription())
                .tags(Arrays.asList(request.getTags().get(0), request.getTags().get(1)))
                .build();

        // Act
        ItemDto actualMappedRequest = itemsMapperPort.mapUpdateItemRequestToDto(request);

        // Assert
        assertThat(expectedMappedRequest.isEqualTo(actualMappedRequest)).isTrue();
    }

    @Test(expected = InvalidItemException.class)
    public void mapUpdateItemDtoToResponseNull() {
        itemsMapperPort.mapUpdateItemDtoToResponse(null);
    }

    @Test
    public void mapUpdateItemDtoToResponse() {
        ItemDto itemToMap = createTestItem();
        UpdateItemResponse mappedItem = itemsMapperPort.mapUpdateItemDtoToResponse(itemToMap);

        assertThat(mappedItem.getId()).isEqualTo(itemToMap.getId());
        assertThat(mappedItem.getName()).isEqualTo(itemToMap.getName());
        assertThat(mappedItem.getDescription()).isEqualTo(itemToMap.getDescription());
        assertThat(mappedItem.getTags()).isNotNull();
        assertThat(mappedItem.getTags().size()).isEqualTo(2);
        assertThat(mappedItem.getTags().get(0)).isEqualTo(itemToMap.getTags().get(0));
        assertThat(mappedItem.getTags().get(1)).isEqualTo(itemToMap.getTags().get(1));
    }

    @Test(expected = InvalidItemException.class)
    public void mapListItemsEntityToDtoNull() {
        itemsMapperPort.mapListItemsEntityToDto(null);
    }

    @Test
    public void mapListItemsEntityToDtoEmpty() {
        Map<String, ItemDto> result = itemsMapperPort.mapListItemsEntityToDto(Collections.emptyList());
        assertThat(result).isNotNull();
        assertThat(result.size()).isLessThan(1);
    }

    @Test
    public void mapListItemsEntityToDto() {
        // Arrange
        ItemEntity itemEntity1 = createTestItemEntity();
        itemEntity1.setId("itemEntity1");

        ItemEntity itemEntity2 = createTestItemEntity();
        itemEntity2.setId("itemEntity2");

        // Act
        Map<String, ItemDto> result = itemsMapperPort.mapListItemsEntityToDto(Arrays.asList(itemEntity1, itemEntity2));

        // Assert
        assertThat(result).isNotNull();
        assertThat(result.size()).isEqualTo(2);
        assertThat(result.keySet().containsAll(Arrays.asList("itemEntity1", "itemEntity2"))).isTrue();
        ItemDto mappedItemDto1 = result.get("itemEntity1");
        ItemDto mappedItemDto2 = result.get("itemEntity2");
        assertThat(mappedItemDto1).isNotNull();
        assertThat(mappedItemDto1.getId()).isEqualTo(itemEntity1.getId());
        assertThat(mappedItemDto1.getName()).isEqualTo(itemEntity1.getName());
        assertThat(mappedItemDto1.getDescription()).isEqualTo(itemEntity1.getDescription());
        assertThat(mappedItemDto1.getTags()).isNotNull();
        assertThat(mappedItemDto1.getTags().size()).isEqualTo(2);
        assertThat(mappedItemDto1.getTags().get(0)).isEqualTo(itemEntity1.getTags().get(0));
        assertThat(mappedItemDto1.getTags().get(1)).isEqualTo(itemEntity1.getTags().get(1));
        assertThat(mappedItemDto2).isNotNull();
        assertThat(mappedItemDto2.getId()).isEqualTo(itemEntity2.getId());
        assertThat(mappedItemDto2.getName()).isEqualTo(itemEntity2.getName());
        assertThat(mappedItemDto2.getDescription()).isEqualTo(itemEntity2.getDescription());
        assertThat(mappedItemDto2.getTags()).isNotNull();
        assertThat(mappedItemDto2.getTags().size()).isEqualTo(2);
        assertThat(mappedItemDto2.getTags().get(0)).isEqualTo(itemEntity2.getTags().get(0));
        assertThat(mappedItemDto2.getTags().get(1)).isEqualTo(itemEntity2.getTags().get(1));

    }

    @Test(expected = InvalidItemException.class)
    public void mapItemEntityToDtoNull() {
        itemsMapperPort.mapItemEntityToDto(null);
    }

    @Test
    public void mapItemEntityToDto() {
        // Arrange
        ItemEntity source = createTestItemEntity();

        // Act
        ItemDto item = itemsMapperPort.mapItemEntityToDto(source);

        // Assert
        assertThat(item).isNotNull();
        assertThat(item.getId()).isEqualTo(source.getId());
        assertThat(item.getName()).isEqualTo(source.getName());
        assertThat(item.getDescription()).isEqualTo(source.getDescription());
        assertThat(item.getTags()).isNotNull();
        assertThat(item.getTags().size()).isEqualTo(source.getTags().size());
        assertThat(item.getTags().get(0)).isEqualTo(source.getTags().get(0));
        assertThat(item.getTags().get(1)).isEqualTo(source.getTags().get(1));
    }

    @Test
    public void mapItemEntityToDtoNullTags() {
        // Arrange
        ItemEntity source = createTestItemEntity();
        source.setTags(null);

        // Act
        ItemDto item = itemsMapperPort.mapItemEntityToDto(source);

        // Assert
        assertThat(item).isNotNull();
        assertThat(item.getId()).isEqualTo(source.getId());
        assertThat(item.getName()).isEqualTo(source.getName());
        assertThat(item.getDescription()).isEqualTo(source.getDescription());
        assertThat(item.getTags()).isNull();
    }

    @Test(expected = InvalidItemException.class)
    public void mapItemDtoToEntityNull() {
        itemsMapperPort.mapItemDtoToEntity(null);
    }

    @Test
    public void mapItemDtoToEntity() {
        // Arrange
        ItemDto source = createTestItem();

        // Act
        ItemEntity item = itemsMapperPort.mapItemDtoToEntity(source);

        // Assert
        assertThat(item).isNotNull();
        assertThat(item.getId()).isEqualTo(source.getId());
        assertThat(item.getName()).isEqualTo(source.getName());
        assertThat(item.getDescription()).isEqualTo(source.getDescription());
        assertThat(item.getTags()).isNotNull();
        assertThat(item.getTags().size()).isEqualTo(source.getTags().size());
        assertThat(item.getTags().get(0)).isEqualTo(source.getTags().get(0));
        assertThat(item.getTags().get(1)).isEqualTo(source.getTags().get(1));
    }

    @Test
    public void mapItemDtoToEntityNullTags() {
        // Arrange
        ItemDto source = createTestItem();
        source.setTags(null);

        // Act
        ItemEntity item = itemsMapperPort.mapItemDtoToEntity(source);

        // Assert
        assertThat(item).isNotNull();
        assertThat(item.getId()).isEqualTo(source.getId());
        assertThat(item.getName()).isEqualTo(source.getName());
        assertThat(item.getDescription()).isEqualTo(source.getDescription());
        assertThat(item.getTags()).isNull();
    }

    private ItemDto createTestItem() {
        return ItemDto
                .builder()
                .id("1234567890")
                .name("testName")
                .description("testDescription")
                .tags(Arrays.asList("tag1", "tag2"))
                .build();
    }

    private ItemEntity createTestItemEntity() {
        return ItemEntity
                .builder()
                .id("1234567890")
                .description("testDescription")
                .name("testName")
                .tags(Arrays.asList("tag1", "tag2"))
                .build();
    }

}
