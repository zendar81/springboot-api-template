package com.jmpr.api.template.configuration;

import com.jmpr.api.template.application.ports.ItemPersistencyPort;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("testWithMocks")
public class ItemsControllerMockedConfiguration {
    @Mock
    private ItemPersistencyPort itemPersistencyPort = Mockito.mock(ItemPersistencyPort.class);

    @Bean
    @Primary
    public ItemPersistencyPort itemPersistencyPort() {
        return itemPersistencyPort;
    }
}
