package com.jmpr.api.template.framework.controller;

import com.jmpr.api.template.application.bean.dto.ItemDto;
import com.jmpr.api.template.application.ports.ItemPersistencyPort;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.StreamUtils;

import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@ActiveProfiles({"test", "test-memory"})
public class ItemsControllerTest {
    private static final String SAMPLE_UUID = "12345678-9012-1234-1234-123456789012";

    @LocalServerPort
    private int port;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ItemsController itemsController;

    @Autowired
    private ItemPersistencyPort persistencyPort;

    @Autowired
    private ApplicationContext context;

    @Test
    public void contextLoads() {
        assertThat(itemsController).isNotNull();
    }

    @Test
    public void getItemsResponse200() throws Exception {
        mockMvc.perform(get("/items"))
                .andDo(print())
                .andExpect(status().is(HttpStatus.OK.value()));
    }

    @Test
    public void getItemsResponse200WithBody() throws Exception {
        String response = StreamUtils.copyToString(context.getResource("classpath:json/responses/getItemsResponse200WithBody.json").getInputStream(), Charset.defaultCharset());

        ItemDto itemToInsert = ItemDto.builder()
                .name("TestName")
                .description("TestDescription")
                .tags(Arrays.asList("tag1", "tag2"))
                .build();

        persistencyPort.storeItem(itemToInsert);

        mockMvc.perform(get("/items"))
                .andDo(print())
                .andExpect(status().is(HttpStatus.OK.value()))
                .andExpect(content().string(org.hamcrest.text.MatchesPattern.matchesPattern(".*\"id\":\".+\".*")))
                .andExpect(content().json(response, false))
        ;
    }

    @Test
    public void postItemResponse201() throws Exception {
        String request = StreamUtils.copyToString(context.getResource("classpath:json/requests/postItemRequest201.json").getInputStream(), Charset.defaultCharset());
        mockMvc.perform(post("/items")
                .content(request)
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(status().is(HttpStatus.CREATED.value()))
                .andExpect(content().string(org.hamcrest.text.MatchesPattern.matchesPattern(".*\"id\":\".+\".*")))
                .andExpect(content().string(org.hamcrest.Matchers.containsString("\"links\":{\"detail\":\"http://localhost:8080")))
        ;
    }

    @Test
    public void postItemResponse400EmptyBody() throws Exception {
        String request = StreamUtils.copyToString(context.getResource("classpath:json/requests/postItemRequest400EmptyBody.json").getInputStream(), Charset.defaultCharset());
        String response = StreamUtils.copyToString(context.getResource("classpath:json/responses/postItemResponse400EmptyBody.json").getInputStream(), Charset.defaultCharset());

        mockMvc.perform(post("/items")
                .content(request)
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andExpect(content().json(response, true))
        ;
    }

    @Test
    public void postItemResponse400NullBody() throws Exception {

        String response = StreamUtils.copyToString(context.getResource("classpath:json/responses/postItemResponse400NullBody.json").getInputStream(), Charset.defaultCharset());

        mockMvc.perform(post("/items")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andExpect(content().json(response, true))
        ;
    }

    @Test
    public void deleteItemResponse204() throws Exception {
        ItemDto itemToInsert = ItemDto.builder()
                .name("TestName")
                .description("TestDescription")
                .tags(Arrays.asList("tag1", "tag2"))
                .build();

        persistencyPort.storeItem(itemToInsert);

        mockMvc.perform(delete("/items/" + itemToInsert.getId())
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(status().is(HttpStatus.NO_CONTENT.value()))
        ;
    }

    @Test
    public void deleteItem404() throws Exception {
        mockMvc.perform(delete("/items/" + UUID.randomUUID())
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(status().is(HttpStatus.NOT_FOUND.value()));
    }

    @Test
    public void deleteItem404WithBody() throws Exception {
        String response = StreamUtils.copyToString(context.getResource("classpath:json/responses/deleteItem404WithBody.json").getInputStream(), Charset.defaultCharset());

        mockMvc.perform(delete("/items/{itemId}", SAMPLE_UUID)
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(status().is(HttpStatus.NOT_FOUND.value()))
                .andExpect(content().json(response, true))
        ;
    }

    @Test
    public void deleteItem400InvalidItemId() throws Exception {
        String response = StreamUtils.copyToString(context.getResource("classpath:json/responses/deleteItem400InvalidItemId.json").getInputStream(), Charset.defaultCharset());


        mockMvc.perform(delete("/items/ABC")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andExpect(content().json(response, true))
        ;
    }

    @Test
    public void getItemDetail200() throws Exception {
        // Arrange
        ItemDto itemToInsert = insertTestItem();

        // Act & Assert
        mockMvc.perform(get("/items/{itemId}", itemToInsert.getId())
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(status().is(HttpStatus.OK.value()))
        ;
    }

    @Test
    public void getItemDetail200WithBody() throws Exception {
        // Arrange
        ItemDto itemToInsert = insertTestItem();
        String response = StreamUtils.copyToString(context.getResource("classpath:json/responses/getItemDetail200WithBody.json").getInputStream(), Charset.defaultCharset());

        // Act & Assert
        mockMvc.perform(get("/items/{itemId}", itemToInsert.getId())
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(status().is(HttpStatus.OK.value()))
                .andExpect(content().json(response, false))
                .andExpect(content().string(org.hamcrest.text.MatchesPattern.matchesPattern(".*\"id\":\".+\".*")))
        ;
    }

    @Test
    public void getItemDetail404() throws Exception {
        mockMvc.perform(get("/items/{itemId}", SAMPLE_UUID).contentType(MediaType.APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(status().is(HttpStatus.NOT_FOUND.value()));
    }

    @Test
    public void getItemDetail400() throws Exception {
        mockMvc.perform(get("/items/{itemId}", "ABC").contentType(MediaType.APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()));
    }

    @Test
    public void getItemDetail400WithBody() throws Exception {
        String response = StreamUtils.copyToString(context.getResource("classpath:json/responses/getItemDetail400WithBody.json").getInputStream(), Charset.defaultCharset());

        mockMvc.perform(get("/items/{itemId}", "ABC").contentType(MediaType.APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andExpect(content().json(response, true))
        ;
    }

    @Test
    public void getItemDetail404WithBody() throws Exception {
        String response = StreamUtils.copyToString(context.getResource("classpath:json/responses/getItemDetail404WithBody.json").getInputStream(), Charset.defaultCharset());

        mockMvc.perform(get("/items/{itemId}", SAMPLE_UUID).contentType(MediaType.APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(status().is(HttpStatus.NOT_FOUND.value()))
                .andExpect(content().json(response, true))
        ;
    }

    @Test
    public void updateItem200() throws Exception {
        String request = StreamUtils.copyToString(context.getResource("classpath:json/requests/updateItem200.json").getInputStream(), Charset.defaultCharset());

        ItemDto insertedItem = insertTestItem();

        mockMvc.perform(put("/items/{itemId}", insertedItem.getId()).contentType(MediaType.APPLICATION_JSON_VALUE).content(request))
                .andDo(print())
                .andExpect(status().is(HttpStatus.OK.value()));
    }

    @Test
    public void updateItem200WithBody() throws Exception {
        String request = StreamUtils.copyToString(context.getResource("classpath:json/requests/updateItem200.json").getInputStream(), Charset.defaultCharset());
        String response = StreamUtils.copyToString(context.getResource("classpath:json/responses/updateItem200WithBody.json").getInputStream(), Charset.defaultCharset());

        ItemDto insertedItem = insertTestItem();

        mockMvc.perform(put("/items/{itemId}", insertedItem.getId()).contentType(MediaType.APPLICATION_JSON_VALUE).content(request))
                .andDo(print())
                .andExpect(status().is(HttpStatus.OK.value()))
                .andExpect(content().json(response, false))
                .andExpect(content().string(org.hamcrest.text.MatchesPattern.matchesPattern(".*\"id\":\".+\".*")))
        ;
    }

    @Test
    public void updateItem404() throws Exception {
        String response = StreamUtils.copyToString(context.getResource("classpath:json/responses/updateItem404.json").getInputStream(), Charset.defaultCharset());


        mockMvc.perform(put("/items/{itemId}", SAMPLE_UUID).contentType(MediaType.APPLICATION_JSON_VALUE).content("{}"))
                .andDo(print())
                .andExpect(status().is(HttpStatus.NOT_FOUND.value()));
    }

    @Test
    public void updateItem400WithoutBody() throws Exception {
        String response = StreamUtils.copyToString(context.getResource("classpath:json/responses/updateItem400WithoutBody.json").getInputStream(), Charset.defaultCharset());

        mockMvc.perform(put("/items/{itemId}", SAMPLE_UUID))
                .andDo(print())
                .andExpect(content().json(response, true))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()));
    }

    @Test
    public void updateItem400WithInvalidItemId() throws Exception {
        String response = StreamUtils.copyToString(context.getResource("classpath:json/responses/updateItem400WithInvalidItemId.json").getInputStream(), Charset.defaultCharset());

        mockMvc.perform(put("/items/{itemId}", "ABC")
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .content("{}")
                )
                .andDo(print())
                .andExpect(content().json(response, true))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()));
    }

    private ItemDto insertTestItem() {
        ItemDto itemToInsert = ItemDto.builder()
                .name("TestName")
                .description("TestDescription")
                .tags(Arrays.asList("tag1", "tag2"))
                .build();

        persistencyPort.storeItem(itemToInsert);
        return itemToInsert;
    }
}
