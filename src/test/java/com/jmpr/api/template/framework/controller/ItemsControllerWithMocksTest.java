package com.jmpr.api.template.framework.controller;

import com.jmpr.api.template.application.ports.ItemPersistencyPort;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.StreamUtils;

import java.nio.charset.Charset;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@ActiveProfiles({"test","testWithMocks"})
public class ItemsControllerWithMocksTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ApplicationContext context;

    @Autowired
    private ItemPersistencyPort itemPersistencyPortMock;

    @Test
    public void internalError() throws Exception {
        Mockito.when(itemPersistencyPortMock.getItems()).thenThrow(new RuntimeException());

        String response = StreamUtils.copyToString(context.getResource("classpath:json/responses/internalError.json").getInputStream(), Charset.defaultCharset());

        mockMvc.perform(get("/items"))
                .andDo(print())
                .andExpect(status().is(HttpStatus.INTERNAL_SERVER_ERROR.value()))
                .andExpect(content().json(response, true))
        ;

        Mockito.verify(itemPersistencyPortMock, Mockito.times(1)).getItems();
    }
}
