package com.jmpr.api.template.domain;

import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 * Domain entity for item API
 *
 * @author Juan Manuel Pérez Rodríguez
 */
@Data
@Builder
public class Item {
    private String id;
    private String name;
    private String description;
    private List<String> tags;
}
