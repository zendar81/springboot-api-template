package com.jmpr.api.template.application.adapters;

import com.jmpr.api.template.application.bean.dto.ItemDto;
import com.jmpr.api.template.application.ports.ItemPersistencyPort;
import com.jmpr.api.template.application.ports.ItemsMapperPort;
import com.jmpr.api.template.framework.entity.ItemEntity;
import com.jmpr.api.template.framework.exception.InvalidItemException;
import com.jmpr.api.template.framework.exception.ItemNotFoundException;
import com.jmpr.api.template.framework.repository.MongoItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

/**
 * Implementation for ItemPersistencyPort that uses mongodb map as persistency mechanism.
 *
 * @author Juan Manuel Pérez Rodríguez
 * @see ItemPersistencyPort
 */
@Service
@ConditionalOnProperty(name = "application.persistency", havingValue = "mongo")
public class MongoItemPersistencyAdapter implements ItemPersistencyPort {
    @Autowired
    private MongoItemRepository mongoItemRepository;

    @Autowired
    private ItemsMapperPort itemsMapperPort;

    @Override
    public ItemDto storeItem(ItemDto itemToStore) {
        if (itemToStore == null) {
            throw new InvalidItemException("Cannot insert a null item");
        }

        if (itemToStore.getId() != null) {
            Optional<ItemEntity> item = mongoItemRepository.findById(itemToStore.getId());
            if (item.isPresent()) {
                throw new InvalidItemException("Can't create an existing item");
            }
        }

        itemToStore.setId(UUID.randomUUID().toString());
        ItemEntity savedItem = mongoItemRepository.save(itemsMapperPort.mapItemDtoToEntity(itemToStore));

        return itemsMapperPort.mapItemEntityToDto(savedItem);
    }

    @Override
    public ItemDto deleteItem(String itemId) {
        if (itemId == null) {
            throw new InvalidItemException("Can't delete a non existent item");
        }

        Optional<ItemEntity> itemToDelete = mongoItemRepository.findById(itemId);

        if (!itemToDelete.isPresent()) {
            throw new ItemNotFoundException("Can't delete a non existent item", itemId);
        }

        mongoItemRepository.deleteById(itemId);

        return itemsMapperPort.mapItemEntityToDto(itemToDelete.get());
    }

    @Override
    public ItemDto getItem(String itemId) {
        if (itemId == null) {
            throw new InvalidItemException("Can't get item detail from null itemId");
        }

        Optional<ItemEntity> target = mongoItemRepository.findById(itemId);


        if (!target.isPresent()) {
            throw new ItemNotFoundException("Item not found", itemId);
        }

        return itemsMapperPort.mapItemEntityToDto(target.get());
    }

    @Override
    public Map<String, ItemDto> getItems() {
        List<ItemEntity> items = mongoItemRepository.findAll();
        return itemsMapperPort.mapListItemsEntityToDto(items);
    }

    @Override
    public ItemDto updateItem(ItemDto itemToUpdate) {
        if (itemToUpdate == null) {
            throw new InvalidItemException("Can't update a null item");
        }

        Optional<ItemEntity> targetItemEntity = mongoItemRepository.findById(itemToUpdate.getId());

        if (!targetItemEntity.isPresent()) {
            throw new ItemNotFoundException("Can't update a non-existent item", itemToUpdate.getId());
        }

        ItemDto targetItemDto = itemsMapperPort.mapItemEntityToDto(targetItemEntity.get());
        ItemDto mergedItem = targetItemDto.mergeWith(itemToUpdate);
        ItemEntity savedItem = mongoItemRepository.save(itemsMapperPort.mapItemDtoToEntity(mergedItem));
        return itemsMapperPort.mapItemEntityToDto(savedItem);
    }
}
