package com.jmpr.api.template.application.ports;

import com.jmpr.api.template.application.bean.dto.ItemDto;

import java.util.Map;

/**
 * This port is a repository for items
 *
 * @author Juan Manuel Pérez Rodríguez
 */
public interface ItemPersistencyPort {
    ItemDto storeItem(ItemDto itemToStore);
    ItemDto deleteItem(String itemId);
    ItemDto getItem(String itemId);
    Map<String, ItemDto> getItems();
    ItemDto updateItem(ItemDto itemToUpdate);
}
