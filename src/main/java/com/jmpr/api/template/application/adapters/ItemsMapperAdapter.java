package com.jmpr.api.template.application.adapters;

import com.jmpr.api.template.application.bean.dto.ItemDto;
import com.jmpr.api.template.application.ports.ItemsMapperPort;
import com.jmpr.api.template.domain.Item;
import com.jmpr.api.template.framework.controller.bean.*;
import com.jmpr.api.template.framework.entity.ItemEntity;
import com.jmpr.api.template.framework.exception.InvalidItemException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Implementation for ItemsMapperPort
 *
 * @author Juan Manuel Pérez Rodríguez
 * @see ItemsMapperPort
 */
@Service
public class ItemsMapperAdapter implements ItemsMapperPort {
    private static final String EMPTY_ITEM_ERROR_MESSAGE = "Cannot map an empty item";
    private static final String CANT_MAP_A_NULL_SOURCE = "Can't map a null source";

    @Value("${application.domain}")
    private String applicationDomain;

    @Override
    public List<ItemDto> mapListItemsResponseToDto(Map<String, ItemDto> items) {
        if (items == null) {
            throw new InvalidItemException("Cannot map an empty item list");
        }

        return items.values().stream().map(item ->
                ItemDto.builder()
                        .id(item.getId())
                        .name(item.getName())
                        .description(item.getDescription())
                        .tags(item.getTags())
                        .build()
        ).collect(Collectors.toList());
    }

    @Override
    public ItemDto mapCreateItemRequestToDto(CreateItemRequest request) {
        if (request == null) {
            throw new InvalidItemException(EMPTY_ITEM_ERROR_MESSAGE);
        }

        return ItemDto
                .builder()
                .name(request.getName())
                .description(request.getDescription())
                .tags(request.getTags())
                .build();
    }

    @Override
    public ItemDto mapUpdateItemRequestToDto(UpdateItemRequest request) {
        if (request == null) {
            throw new InvalidItemException("Can't map a null source item");
        }

        return ItemDto.builder()
                .name(request.getName())
                .description(request.getDescription())
                .tags(request.getTags())
                .build();
    }

    @Override
    public CreateItemResponse mapCreateItemDtoToResponse(ItemDto response) {
        if (response == null) {
            throw new InvalidItemException(EMPTY_ITEM_ERROR_MESSAGE);
        }

        return CreateItemResponse
                .childBuilder()
                .id(response.getId())
                .name(response.getName())
                .description(response.getDescription())
                .links(CreateItemResponseLinks.builder().detail(String.format("%s/items/%s", applicationDomain, response.getId())).build())
                .childBuild();
    }

    @Override
    public Item mapGetItemDetailDto(ItemDto response) {
        if (response == null) {
            throw new InvalidItemException(EMPTY_ITEM_ERROR_MESSAGE);
        }

        return Item.builder()
                .id(response.getId())
                .description(response.getDescription())
                .name(response.getName())
                .tags(response.getTags())
                .build();
    }

    @Override
    public Map<String, ItemDto> mapListItemsEntityToDto(List<ItemEntity> items) {
        if (items == null) {
            throw new InvalidItemException("Can't map a null input");
        }

        return items.stream().map(item -> ItemDto
                .builder()
                .id(item.getId())
                .description(item.getDescription())
                .name(item.getName())
                .tags(item.getTags())
                .build()).collect(Collectors.toMap(ItemDto::getId, item -> item));
    }

    @Override
    public UpdateItemResponse mapUpdateItemDtoToResponse(ItemDto response) {
        if (response == null) {
            throw new InvalidItemException(CANT_MAP_A_NULL_SOURCE);
        }
        return UpdateItemResponse
                .childBuilder()
                .id(response.getId())
                .name(response.getName())
                .description(response.getDescription())
                .tags(response.getTags())
                .childBuild();
    }

    @Override
    public ItemDto mapItemEntityToDto(ItemEntity source) {
        if (source == null) {
            throw new InvalidItemException(CANT_MAP_A_NULL_SOURCE);
        }

        return ItemDto
                .builder()
                .id(source.getId())
                .name(source.getName())
                .description(source.getDescription())
                .tags(source.getTags() == null ? null : new ArrayList<>(source.getTags()))
                .build();
    }

    @Override
    public ItemEntity mapItemDtoToEntity(ItemDto source) {
        if (source == null) {
            throw new InvalidItemException(CANT_MAP_A_NULL_SOURCE);
        }

        return ItemEntity
                .builder()
                .id(source.getId())
                .name(source.getName())
                .description(source.getDescription())
                .tags(source.getTags() == null ? null : new ArrayList<>(source.getTags()))
                .build();
    }
}
