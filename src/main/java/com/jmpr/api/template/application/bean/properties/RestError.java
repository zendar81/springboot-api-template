package com.jmpr.api.template.application.bean.properties;

import lombok.Data;

/**
 * POJO to map controller error properties in application.yml
 *
 * @author Juan Manuel Pérez Rodríguez
 */
@Data
public class RestError {
    private String status;
    private String code;
    private String message;
}
