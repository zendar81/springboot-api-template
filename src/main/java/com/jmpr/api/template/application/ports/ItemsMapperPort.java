package com.jmpr.api.template.application.ports;

import com.jmpr.api.template.application.bean.dto.ItemDto;
import com.jmpr.api.template.domain.Item;
import com.jmpr.api.template.framework.controller.bean.CreateItemRequest;
import com.jmpr.api.template.framework.controller.bean.CreateItemResponse;
import com.jmpr.api.template.framework.controller.bean.UpdateItemRequest;
import com.jmpr.api.template.framework.controller.bean.UpdateItemResponse;
import com.jmpr.api.template.framework.entity.ItemEntity;

import java.util.List;
import java.util.Map;

/**
 * Port to map ItemDto to controller beans in requests/responses
 *
 * @author Juan Manuel Pérez Rodríguez
 */
public interface ItemsMapperPort {
    List<ItemDto> mapListItemsResponseToDto(Map<String, ItemDto> serviceItems);
    ItemDto mapCreateItemRequestToDto(CreateItemRequest request);
    ItemDto mapUpdateItemRequestToDto(UpdateItemRequest request);
    CreateItemResponse mapCreateItemDtoToResponse(ItemDto response);
    UpdateItemResponse mapUpdateItemDtoToResponse(ItemDto response);
    Item mapGetItemDetailDto(ItemDto response);
    Map<String, ItemDto> mapListItemsEntityToDto(List<ItemEntity> items);
    ItemDto mapItemEntityToDto(ItemEntity source);
    ItemEntity mapItemDtoToEntity(ItemDto source);
}
