package com.jmpr.api.template.application.adapters;

import com.jmpr.api.template.application.ports.ItemPersistencyPort;
import com.jmpr.api.template.application.bean.dto.ItemDto;
import com.jmpr.api.template.framework.exception.InvalidItemException;
import com.jmpr.api.template.framework.exception.ItemNotFoundException;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Implementation for ItemPersistencyPort that uses an in-memory map as persistency mechanism.
 *
 * @author Juan Manuel Pérez Rodríguez
 * @see com.jmpr.api.template.application.ports.ItemPersistencyPort
 */
@Service
@ConditionalOnProperty(name = "application.persistency", havingValue = "memory")
public class InMemoryItemPersistencyAdapter implements ItemPersistencyPort {
    private Map<String, ItemDto> items;

    public InMemoryItemPersistencyAdapter() {
        this.items = new HashMap<>();
    }


    @Override
    public ItemDto storeItem(ItemDto itemToStore) {
        if (itemToStore == null) {
            throw new InvalidItemException("Cannot insert a null item");
        }

        itemToStore.setId(UUID.randomUUID().toString());
        items.put(itemToStore.getId(), itemToStore);

        return itemToStore;
    }

    @Override
    public ItemDto deleteItem(String itemId) {
        if (itemId == null) {
            throw new InvalidItemException("Can't delete a non existent item");
        }

        ItemDto itemToDelete = items.get(itemId);

        if (itemToDelete == null) {
            throw new ItemNotFoundException("Can't delete a non existent item", itemId);
        } else {
            items.remove(itemId);
            itemToDelete.setId(null);
        }

        return itemToDelete;
    }

    @Override
    public ItemDto getItem(String itemId) {
        if (itemId == null) {
            throw new InvalidItemException("Can't get item detail from null itemId");
        }

        ItemDto itemToGet = items.get(itemId);

        if (itemToGet == null) {
            throw new ItemNotFoundException("Item not found", itemId);
        } else {
            return itemToGet;
        }
    }

    @Override
    public Map<String, ItemDto> getItems() {
        return items;
    }

    @Override
    public ItemDto updateItem(ItemDto itemToUpdate) {
        if (itemToUpdate == null) {
            throw new InvalidItemException("Can't update a null item");
        }

        ItemDto targetItem = items.get(itemToUpdate.getId());

        if (targetItem == null) {
            throw new ItemNotFoundException("Can't update a non existing item", itemToUpdate.getId());
        }

        ItemDto mergedItem = targetItem.mergeWith(itemToUpdate);
        items.put(itemToUpdate.getId(), mergedItem);

        return new ItemDto(mergedItem);
    }
}
