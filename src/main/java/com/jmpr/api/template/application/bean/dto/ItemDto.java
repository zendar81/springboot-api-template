package com.jmpr.api.template.application.bean.dto;

import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 * Item DTO for application layer
 *
 * @author Juan Manuel Pérez Rodríguez
 */
@Data
@Builder
public class ItemDto {
    private String id;
    private String name;
    private String description;
    private List<String> tags;

    public ItemDto(String id, String name, String description, List<String> tags) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.tags = tags;
    }

    public ItemDto(ItemDto source) {
        this.id = source.getId();
        this.name = source.getName();
        this.description = source.getDescription();
        this.tags = source.tags;
    }

    public ItemDto mergeWith(ItemDto source) {
        if (source == null) {
            return this;
        }

        ItemDto result = ItemDto.builder().build();

        result.setId(source.getId() == null ? this.id : source.getId());
        result.setName(source.getName() == null ? this.name : source.getName());
        result.setDescription(source.getDescription() == null ? this.description : source.getDescription());
        result.setTags(source.getTags() == null ? this.tags : source.getTags());

        return result;
    }

    public boolean isEqualTo(ItemDto target) {
        if (target == null) {
            return false;
        }

        if (target.getId() != this.id) {
            return false;
        }

        if (target.getName() != this.name) {
            return false;
        }

        if (target.getDescription() != this.getDescription()) {
            return false;
        }

        // If only one of both tag arrays are null, equality fails
        if (target.getTags() != null ^ this.getTags() != null) {
            return false;
        }

        if (target.getTags() == null) {
            return true;
        } else {
            return target.getTags().containsAll(this.tags);
        }
    }
}
