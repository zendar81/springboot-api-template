package com.jmpr.api.template.framework.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Exception thrown when an invalid request is sent
 *
 * @author Juan Manuel Pérez Rodríguez
 */
@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Invalid request")
public class InvalidItemException extends RuntimeException {
    public InvalidItemException(String message) {
        super(message);
    }
}
