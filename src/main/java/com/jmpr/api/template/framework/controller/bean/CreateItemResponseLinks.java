package com.jmpr.api.template.framework.controller.bean;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 * Class modelling links object inside CreateItemResponse
 *
 * @author Juan Manuel Pérez Rodríguez
 */
@Getter
@Setter
@Builder
public class CreateItemResponseLinks {
    private String detail;
}
