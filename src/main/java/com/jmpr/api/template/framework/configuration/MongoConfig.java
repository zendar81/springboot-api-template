package com.jmpr.api.template.framework.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@Configuration
@EnableMongoRepositories(basePackages = {"com.jmpr.api.template.framework.repository"})
public class MongoConfig {
}
