package com.jmpr.api.template.framework.controller.bean;

import com.jmpr.api.template.application.bean.dto.ItemDto;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * Root class for update item response
 *
 * @author Juan Manuel Pérez Rodríguez
 */
@Getter
@Setter
@EqualsAndHashCode
public class UpdateItemResponse extends ItemDto {
    // This is a workaround to be able to use @Builder on subclasses
    // (see https://www.baeldung.com/lombok-builder-inheritance)
    @Builder(buildMethodName = "childBuild", builderMethodName = "childBuilder")
    public UpdateItemResponse(String name, String description, List<String> tags, String id, CreateItemResponseLinks links) {
        super(id, name, description, tags);
    }
}
