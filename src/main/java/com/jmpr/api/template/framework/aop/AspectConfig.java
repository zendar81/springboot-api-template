package com.jmpr.api.template.framework.aop;

import lombok.extern.log4j.Log4j2;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;

/**
 * Spring configuration to add a log trace when executing controller methods via AOP
 *
 * @author Juan Manuel Pérez Rodríguez
 */
@Aspect
@Configuration
@Log4j2
public class AspectConfig {
    @Before("execution(* com.jmpr.api.template.framework.controller.*.*(..))")
    public void before(JoinPoint joinPoint) {
        log.info("Entered {}.{}() with arguments : {}",
                joinPoint.getTarget().getClass().getSimpleName(),
                joinPoint.getSignature().getName(),
                Arrays.toString(joinPoint.getArgs()));
    }
}
