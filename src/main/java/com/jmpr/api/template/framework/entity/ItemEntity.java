package com.jmpr.api.template.framework.entity;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Data
@Builder
@Document(collection = "items")
public class ItemEntity {
    @Id
    private String id;
    private String name;
    private String description;
    private List<String> tags;
}
