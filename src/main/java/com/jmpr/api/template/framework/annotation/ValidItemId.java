package com.jmpr.api.template.framework.annotation;

import com.jmpr.api.template.framework.validators.ValidItemIdValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * This is a custom annotation to validate item identifiers
 *
 * @author Juan Manuel Pérez Rodríguez
 */
@Constraint(validatedBy = ValidItemIdValidator.class)
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.PARAMETER)
public @interface ValidItemId {
    String message() default  "invalidItemId||Invalid value for itemId : {}";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
