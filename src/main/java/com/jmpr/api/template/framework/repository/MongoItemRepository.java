package com.jmpr.api.template.framework.repository;

import com.jmpr.api.template.framework.entity.ItemEntity;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
@ConditionalOnProperty(name = "application.persistency", havingValue = "mongo")
public interface MongoItemRepository extends MongoRepository<ItemEntity, String> {
}
