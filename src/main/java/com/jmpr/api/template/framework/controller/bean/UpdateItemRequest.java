package com.jmpr.api.template.framework.controller.bean;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Root class for update item request
 *
 * @author Juan Manuel Pérez Rodríguez
 */
@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
@AllArgsConstructor
public class UpdateItemRequest {
    @NotNull private String name;
    @NotNull private String description;
    private List<String> tags;
}
