package com.jmpr.api.template.framework.controller.bean;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 * Root object for error responses in controllers
 *
 * @author Juan Manuel Pérez Rodríguez
 */
@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ErrorResponse {
    private List<ErrorResponseErrors> errors;
}
