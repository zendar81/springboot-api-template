package com.jmpr.api.template.framework.controller;

import com.jmpr.api.template.framework.controller.bean.StatusResponse;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller for root endpoint
 *
 * @author Juan Manuel Pérez Rodríguez
 */
@RestController
@Log4j2
public class RootController {
    @RequestMapping(path = "/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public StatusResponse root() {
        return StatusResponse.builder().status("OK").build();
    }
}