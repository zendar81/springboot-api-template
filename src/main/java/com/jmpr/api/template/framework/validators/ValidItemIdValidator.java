package com.jmpr.api.template.framework.validators;

import com.jmpr.api.template.framework.annotation.ValidItemId;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * This is a validator for item identifiers. It triggers with @ValidItemId annotation
 *
 * @author Juan Manuel Pérez Rodríguez
 * @see com.jmpr.api.template.framework.annotation.ValidItemId
 */
public class ValidItemIdValidator implements ConstraintValidator<ValidItemId, String> {
    public static final String UUID_REGEX = "([a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}){1}";

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        return s.matches(UUID_REGEX);
    }
}
