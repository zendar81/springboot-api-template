package com.jmpr.api.template.framework.exception.handlers;

import com.jmpr.api.template.application.bean.properties.RestError;
import com.jmpr.api.template.framework.controller.bean.ErrorResponse;
import com.jmpr.api.template.framework.controller.bean.ErrorResponseErrors;
import com.jmpr.api.template.framework.exception.ItemNotFoundException;
import com.jmpr.api.template.framework.properties.RestErrorMessages;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * This class captures exceptions in controllers and send a proper response for each case
 *
 * @author Juan Manuel Pérez Rodríguez
 */
@RestControllerAdvice
@Log4j2
public class MainControllerAdvice {
    public static final String INTERNAL_ERROR_KEY = "InternalServerError";

    @Autowired
    private RestErrorMessages restErrorMessages;

    @ExceptionHandler(HttpMessageNotReadableException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<ErrorResponse> httpMessageNotReadableExceptionHandler(HttpMessageNotReadableException ex) {
        log.error(ex);
        return buildError(ex.getClass().getSimpleName(), null);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<ErrorResponse> methodArgumentNotValidHandler(MethodArgumentNotValidException ex) {
        log.error(ex);
        return buildError(ex.getClass().getSimpleName(), null);
    }

    @ExceptionHandler(ItemNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity<ErrorResponse> itemNotFoundExceptionHandler(ItemNotFoundException ex) {
        log.error(ex);

        return buildError(ex.getClass().getSimpleName(), ex.getInvalidValue());
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<ErrorResponse> constraintViolationExceptionHandler(ConstraintViolationException ex) {
        log.error(ex);

        List<ErrorResponseErrors> errorList = ex.getConstraintViolations().stream().map(this::mapViolation).collect(Collectors.toList());

        return ResponseEntity
                .status(HttpStatus.valueOf(HttpStatus.BAD_REQUEST.value()))
                .body(ErrorResponse.builder()
                        .errors(errorList)
                        .build());
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ResponseEntity<ErrorResponse> exceptionHandler(Exception ex) {
        log.error("Unhandled exception", ex);

        return buildError(INTERNAL_ERROR_KEY, null);
    }

    private ResponseEntity<ErrorResponse> buildError(String simpleName, String invalidValue) {
        RestError error = restErrorMessages.getErrors().get(simpleName);

        return ResponseEntity
                .status(HttpStatus.valueOf(error.getStatus()))
                .body(ErrorResponse.builder().errors(
                        Arrays.asList(ErrorResponseErrors.builder()
                                .code(error.getCode())
                                .message(error.getMessage().replaceAll("_invalidValue_", invalidValue == null ? "" : invalidValue))
                                .build())
                ).build());
    }

    private ErrorResponseErrors mapViolation(ConstraintViolation source) {
        String[] messageParts = source.getMessage().split("\\|\\|");
        return

                ErrorResponseErrors.builder()
                        .code(messageParts[0])
                        .message(messageParts[1].replaceAll("\\{\\}", source.getInvalidValue().toString()))
                        .build();
    }
}
