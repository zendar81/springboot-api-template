package com.jmpr.api.template.framework.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Exception thrown when an item is not found
 *
 * @author Juan Manuel Pérez Rodríguez
 */
@Getter
@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Item not found")
public class ItemNotFoundException extends RuntimeException {
    private final String invalidValue;

    public ItemNotFoundException(String message, String invalidValue) {
        super(message);
        this.invalidValue = invalidValue;
    }
}
