package com.jmpr.api.template.framework.controller.bean;

import com.jmpr.api.template.application.bean.dto.ItemDto;
import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 * Root class for List items response
 *
 * @author Juan Manuel Pérez Rodríguez
 */
@Data
@Builder
public class ListItemsResponse {
    private List<ItemDto> items;
}
