package com.jmpr.api.template.framework.properties;

import com.jmpr.api.template.application.bean.properties.RestError;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * This class maps error messages from application properties
 *
 * @author Juan Manuel Pérez Rodríguez
 */
@Data
@ConfigurationProperties(prefix = "rest")
@Component
public class RestErrorMessages {
    private Map<String, RestError> errors;
}
