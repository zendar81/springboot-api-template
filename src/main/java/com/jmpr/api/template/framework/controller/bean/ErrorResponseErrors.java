package com.jmpr.api.template.framework.controller.bean;

import lombok.Builder;
import lombok.Data;

/**
 * 'errors' node inside error response for controllers
 *
 * @author Juan Manuel Pérez Rodríguez
 */
@Data
@Builder
public class ErrorResponseErrors {
    private String code;
    private String message;
}
