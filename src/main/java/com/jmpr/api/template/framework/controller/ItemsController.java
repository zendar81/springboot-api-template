package com.jmpr.api.template.framework.controller;

import com.jmpr.api.template.application.ports.ItemPersistencyPort;
import com.jmpr.api.template.application.ports.ItemsMapperPort;
import com.jmpr.api.template.application.bean.dto.ItemDto;
import com.jmpr.api.template.domain.Item;
import com.jmpr.api.template.framework.annotation.ValidItemId;
import com.jmpr.api.template.framework.controller.bean.*;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

/**
 * Controller for Item operations
 *
 * @author Juan Manuel Pérez Rodríguez
 */
@RestController
@Log4j2
@RequestMapping(path = "/items")
@Validated
public class ItemsController {
    @Autowired
    private ItemPersistencyPort persistency;

    @Autowired
    private ItemsMapperPort mapper;

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ListItemsResponse listItems() {
        Map<String, ItemDto> items = persistency.getItems();
        List<ItemDto> itemDtos = mapper.mapListItemsResponseToDto(items);

        return ListItemsResponse.builder()
                .items(itemDtos)
                .build();
    }

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(code = HttpStatus.CREATED)
    public CreateItemResponse createItem(@RequestBody @Valid CreateItemRequest request) {
        ItemDto itemDto = mapper.mapCreateItemRequestToDto(request);
        persistency.storeItem(itemDto);
        return mapper.mapCreateItemDtoToResponse(itemDto);

    }

    @RequestMapping(path = "/{itemId}", method = RequestMethod.DELETE)
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void deleteItem(@PathVariable @ValidItemId String itemId) {
        persistency.deleteItem(itemId);
    }

    @RequestMapping(path = "/{itemId}", method = RequestMethod.GET)
    public Item getItemDetail(@PathVariable @ValidItemId String itemId) {
        ItemDto itemDto = persistency.getItem(itemId);
        return mapper.mapGetItemDetailDto(itemDto);
    }

    @RequestMapping(path = "/{itemId}", method = RequestMethod.PUT)
    @ResponseStatus(code = HttpStatus.OK)
    public UpdateItemResponse updateItem(@PathVariable @ValidItemId String itemId, @RequestBody UpdateItemRequest request) {
        ItemDto mappedRequest = mapper.mapUpdateItemRequestToDto(request);
        mappedRequest.setId(itemId);
        ItemDto storedItem = persistency.updateItem(mappedRequest);
        return mapper.mapUpdateItemDtoToResponse(storedItem);
    }
}
