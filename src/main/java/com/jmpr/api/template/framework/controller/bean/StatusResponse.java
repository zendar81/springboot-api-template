package com.jmpr.api.template.framework.controller.bean;

import lombok.Builder;
import lombok.Data;

/**
 * Class modelling status response
 *
 * @author Juan Manuel Pérez Rodríguez
 */
@Data
@Builder
public class StatusResponse {
    private String status;
}
