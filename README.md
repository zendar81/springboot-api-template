# Spring Boot Api Template

## Index

1. [About this project](#1-about-this-project)
2. [API definition](#2-api-definition)
3. [Features](#3-features)
4. [Project structure](#4-project-structure)
5. [Running the API](#5-running-the-api)
6. [Sonarqube reports](#6-sonarqube-reports)



## 1. About this project

This project is just a template for a sample REST API using Java and Spring Boot.

It was developed using TDD, aiming to have an excellent test coverage.

## 2. API definition

Find API definition [here](https://apitemplate.docs.apiary.io/#)

## 3. Features

- REST API for CRUD operations over a generic entity "item"
- AOP for logging in controller methods
- Lombok annotations
- Spring profiles and conditional bean injection
- 2 persistency modes
  - In memory
  - MongoDB
- Unitary/integration tests using:
  - Junit
  - AssertJ
  - Embedded mongo
- Errors in API handled through ControllerAdvices
- Error responses parameterized via properties
- Custom object validation through annotations
- Sonarqube configuration

## 4. Project structure

The project includes the following:

Path    | Filename  | Desription
--------|-----------|-----------
root    | README.md | Project documentation
root    | pom.xml   | Maven configuration file
root    | Dockerfile    | Docker configuration file needed for building docker image
root    | DockerfileWithMongo    | Docker configuration file needed for building docker image with mongo support
root    | lombok.config | Lombok configuration file
/src    | *         | Source files
/postman    | *     | Postman collections & environment
/docker | *     | Docker compose files and scripts
/k8s | * | Kubernetes files to run the API in k8s

## 5. Running the API

### 5.1 In memory persistency mode

#### 5.1.1 With docker-compose

- Build the artifact

```bash
mvn clean install
```

- Run docker-compose up

```bash
cd docker-compose
docker-compose build
docker-compose up
``` 

**Note** To tear down the application, hit CTRL+C or run docker-compose down

- Navigate to root endpoint or use postman collections to invoke API

```bash
http://localhost:8080
```

#### 5.1.2 With docker

- Build docker image

```bash
docker build . -t springboot-api-template
``` 

- Run docker image

```bash
docker run -d -p 8080:8080 sprinboot-api-template
```

### 5.2 MongoDB persistency mode

#### 5.2.1 With docker compose
- Build the artifact

```bash
mvn clean install
```

- run docker-compose up using mongodb docker compose file and **specify your mongodb connection string**. For example, *mongodb+srv://user:pwd@host/items?retryWrites=true*

```bash
cd docker
docker-compose -f docker-compose-mongo.yml build --build-arg mongodbUri=<yourMongoDbConnectionString>
docker-compose up
```

**Note** To tear down the application, hit CTRL+C or run docker-compose down

- Navigate to root endpoint or use postman collections to invoke API

```bash
http://localhost:8080
```

#### 5.2.2 With docker

- Build docker image

```bash
docker build . -t springboot-api-template
```

- Run docker image

```bash
docker run -d -p 8080:8080 sprinboot-api-template
```
## 5.3 Running in k8s (minikube)

To run the API in kubernetes, we will use minikube for simplicity.

Refer to [documentation](https://kubernetes.io/docs/tasks/tools/install-minikube/) to install minikube in your system. Also, I assume that you have kubectl installed.

1. Start minikube
2. Go to /k8s directory inside the project
3. Deploy the API in your cluster using deployment.yml file

```bash
$ kubectl apply -f deployment.yml
```

4. Deploy the service to make pods available outside of cluster

```bash
$ kubectl apply -f service.yml
```

5. Deploy the ingress to access the API through a specific domain. You need to enable the ingress addon in minikube, that is disabled by default

```bash
$ minikube addon enable ingress
ingress was successfully enabled
$ kubectl apply -f ingress.yml
```

6. Link 'jmpr-api' domain to your minikube IP

```bash
$ echo "$(minikube ip) jmpr-api" | sudo tee -a /etc/hosts
```

Alternatively if you have no vm driver for minikube, you can get the IP for your service using

```bash
$ minikube service --url api-service
```

7. Inspect cluster to check that everything is ok

```bash
$ minikube dashboard
```

8. Use postman collection with LOC K8S environment to access the API

## 6. Sonarqube reports

1. Install docker
2. Pull docker image

```bash
docker pull sonarqube
```

3. Run sonarqube container

```bash
docker run -d --name sonarqube -p 9000:9000 -p 9092:9092 sonarqube
```

4. Wait until container is ready, navigate to localhost:9000 and login with admin/admin.

5. Generate a token inside UI to use in analysis, we will refer to it as <token>

6. Analyse the project

```bash
mvn sonar:sonar \
  -Dsonar.host.url=http://localhost:9000 \
  -Dsonar.login=[token]
```

7. Inspect the results in the UI
