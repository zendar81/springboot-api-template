FROM openjdk:8-slim AS spring-boot-api-template
LABEL maintainer="Juan Manuel Pérez Rodríguez"
EXPOSE 8080:8080
COPY target/spring-boot-apitemplate-0.0.1-SNAPSHOT.jar /code/application.jar
ENTRYPOINT java -jar -Dspring.profiles.active=live,memory /code/application.jar
