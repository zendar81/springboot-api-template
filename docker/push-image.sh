#!/usr/bin/env bash
if [[ $# -lt 3 ]] ; then
    echo 'Invalid parameters, please read below'
    echo
    echo 'Usage: push-image.sh <repository> <image> <tag>'
    exit 0
fi

docker tag $2:$3 jmpr1981/$2:$3
docker push jmpr1981/$2:$3
docker rmi -f jmpr1981/$2:$3
